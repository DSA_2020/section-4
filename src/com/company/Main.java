package com.company;
import com.company.checker.BalancedBrackets;
/**
 * The Check_Brackets program implements an application that
 * simply checks if a string of brackets given by the user
 * is balanced or not.
 *
 * @author  Mohamed Sherif
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {
	   // Check for the validity of userInput brackets
        String userInput = "()(";
       BalancedBrackets checkBrackets = new BalancedBrackets(userInput);
       System.out.println(checkBrackets.checkValid());
    }
}
