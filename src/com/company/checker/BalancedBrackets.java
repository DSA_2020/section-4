package com.company.checker;
import com.company.stack.Stack;
/**
 * The class checks for the validity of the user's input brackets
 * there are 4 cases where the input can be in-valid:
 * 1).if there is an extra bracket at the end of the stack
 * 2).if there is an extra bracket at the beginning of the stack
 * 3).if there are unmatched bracket types :"(]"
 * 4).the opening bracket has a no ending bracket and vise-versa
 *
 * @author Mohamed Sherif
 */
public class BalancedBrackets {
    private String expr;
    private Stack validationStack;
    /*
     *Creates a linked-list stack to support the validation operation
     */
    public BalancedBrackets(String expr) {
        this.expr = expr;
        this.validationStack = new Stack();
    }
    /*Check for Vailed matching (),{},[]
     * @param  C1 Defines the current closed bracket "),].}"
     * @param  C2 Define the last inserted opening bracket in the stack "(,[.{"
     * @return True if a valid match is detected
     */
    private boolean isMatching(char C1,char C2){
        if (C1 == '('&& C2 ==')')
            return true;
        if (C1 == '{'&& C2 =='}')
            return true;
        if (C1 == '['&& C2 ==']')
            return true;
        //unmatched case detected
        return false;
    }
    /*
      * Performs the validation check over the input string
      * the only valid case is if the stcak is empty after
      * all of the characters passes the is matching check
      * @return  True only if the input string of brackets is valid
     */
    public boolean checkValid(){
        //Looping over each character in the input string
        for (int i =0; i<expr.length();i++){
            //if an open bracket found then insert it to the stack
            if (expr.charAt(i) == '{' || expr.charAt(i) == '(' || expr.charAt(i) == '[')
                validationStack.push(expr.charAt(i));
            //if a closed bracket found the match it with the last open bracket inserted inside the stack
            else if (expr.charAt(i) == '}' || expr.charAt(i) == ')' || expr.charAt(i) == ']') {
                //NOTE: if stack is empty while iterating the string that means that there is a closed
                //      bracket that doesnt have a opened one")"
                if (validationStack.isEmpty() || !isMatching(validationStack.pop(), expr.charAt(i)))
                    return false;
            }
        }
        //if stack is full after we finished the string then not valid and valid other wise
        return validationStack.isEmpty();
    }
}
