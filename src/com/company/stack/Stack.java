package com.company.stack;
/**
 * The class implements stack data structure that will
 * be used to store the characters of the input string
 *
 * @author Mohamed Sherif
 */
public class Stack {
    Node head;
    private class Node{
        private char value;
        private Node next;
        /*
          * Creates a new node with the specified value
          * @param value The value that will be stored inside the node
         */
        public Node(char value) {
            this.value = value;
            //the next pointer of a node points to null by default
            this.next = null;
        }
    }
    /*
      * Creates a new node and inserts it before the last node
      * to create first-in-last-out(FILO) concept
      * @param data Value that will be stored inside the new created node.
     */
    public void push(char data){
        Node newNode = new Node(data);
        //If the new created node is the first node in the stack,then make the head
        //pointer points over it
        if (head == null)
            head = newNode;
        else {
            //If the new node isnt the first node in the stack, then links it to the node before
            newNode.next = head;
            //Point the head pointer to the last inserted node.
            head = newNode;
        }
    }
    /*
     * Removes the last node in stack and returns its value
     * @return   The value of the last node in the stack
     */
    public char pop(){
        char data = head.value;
        //change pointer to point the node before
        head = head.next;
        return  data;
    }
    /*
     *Checks if stack has any value stored inside it or not
     * @return   True if empty
     */
    public boolean isEmpty(){ return (head == null);}
}
